#!/usr/bin/perl


use strict;
use warnings;
use Data::Dumper;

#read config file 
 
my $config_file = 'config.cfg';

my %configParamHash = ();
open ( _FH, $config_file ) or die "Unable to open config file: $!";
 
while ( <_FH> ) {
    chomp;
    s/#.*//;                # ignore comments
    s/^\s+//;               # trim leading spaces if any
    s/\s+$//;               # trim leading spaces if any
    next unless length;
    my ($_configParam, $_paramValue) = split(/\s*=\s*/, $_, 2);
    $configParamHash{$_configParam} = $_paramValue;
}
close _FH;

# TODO check if not missing config vars
# Summary & clear screen
system 'clear';
my $min_subworker = $configParamHash{min_subworker};
my $max_subworker = $configParamHash{max_subworker};
my $idle_subworker = $configParamHash{idle_subworker};
my $idle_message = $configParamHash{idle_message};
my $session_runtime = $configParamHash{session_run};
my $pool_address = $configParamHash{pool_address};
my $log_file = $configParamHash{log_name};

#power mode :D
my $start_hour = $configParamHash{s_hour};
my $end_hour = $configParamHash{e_hour};
my $maxed_subworker = $configParamHash{maxed_subworker};


# other vars

my $log_command = "Started subworker no: ";


print "###  RMSD (random minerd subworker dispatcher) STARTED ###\n";
print "Min Subworkers: $min_subworker\n";
print "Max Subworkers: $max_subworker\n";

print "Idle Subworker No: $idle_subworker\n";
print "Idle Message Log: $idle_message\n";
print "Time for a session: $session_runtime seconds\n";
print "Pool Address Command: $pool_address\n";
print "############## gl, many yays! ############################\n";

# create a log file


open my $fileHandle, ">>", "$log_file" or die "Can't open '$log_file'\n";
print "Log file $log_file created!\n";
close $fileHandle;


	
my $continue = 1;
$SIG{TERM} = sub { $continue = 0 };



while ($continue) 

{
    my $script_no = int($min_subworker + rand($max_subworker));
    my $cmd;
    my  $now = localtime; 
    
    
    my $hour = (localtime)[2];
    
  
    

    
if ( $hour >= $start_hour and $hour < $end_hour ) { 

$script_no = $maxed_subworker;

 }    
 
 
    if($script_no == $idle_subworker)
    
    {
    #give some time to sleep
    $cmd = "";
    system ("echo $now '... subworker sleeps... ' $idle_message >> $log_file");
    }
    
    else {
    
     $cmd = "$pool_address".$script_no." &";

    
    }

	   
system ("echo $now' » Started Subworker' $script_no >> $log_file");



system ($cmd);

   
wait;
    # sleep(900); # 1800 = 30 min / 900 = 15 min
sleep($session_runtime);
 

system("pkill -9 -f ./minerd");
	  
	

system ("echo $now ' « Ended Subworker:' $script_no >> $log_file");



	
}	


exit;


