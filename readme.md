# RMSD (Minerd)
#### version 1.1


RMSD was written for my personal purposes. I want to keep my computer (I don't own any mining devices) from being cooked up. What RMSD can do:

  - run sessions with defined number of threads
  - log activity 
  - help you to keep your hardware (CPU) away from a suicide mode (if anybody cares)
 
The setup was tested on Ubuntu, Centos and SL.  
  

#### Pre

Make sure you have:

* [perl]
* [cpuminer] - https://github.com/pooler/cpuminer
* [editable firewall]

#### Installation
* Get cpuminer 
* make sure minerd (binary) and run.pl are in the same place
* rename config.cfg_sample to config.cfg
* edit config.cfg as you need
* run command:
```
perl run.pl
```


#### Todo's
I have some ideas but, I don't have time

License
----

MIT

**Like it? Coffee donation :D**
```
BTC: 17wyCqb8X2hRhNXCtnXa9DXXQShYUJmT2G
```




